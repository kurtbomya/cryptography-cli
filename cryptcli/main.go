/*
Copyright © 2024 KURT BOMYA
*/
package main

import "gitlab.com/kurtbomya/cryptography_cli/cryptcli/cmd"

func main() {
	crypto_cli_commands.Execute()
}
