/*
Copyright © 2024 KURT BOMYA
*/
package crypto_cli_commands

import (
	"fmt"
	"github.com/spf13/cobra"
	"strings"
)

// caesarCmd represents the caesar command
var caesarCmd = &cobra.Command{
	Use:   "caesar",
	Short: "Apply the caesar cipher to a message and return it",
	Long: `This flag takes a string that you want to encrypt as 
well as how many characters you want to shift the caesar cipher 
and returns the modified string`,
	Run: func(cmd *cobra.Command, args []string) {
		message, err := cmd.Flags().GetString("message")
		if err != nil {
			fmt.Println(err)
		}

		characterShift, err := cmd.Flags().GetInt("shift")
		if err != nil {
			fmt.Println(err)
		}

		encryptedMessage := caesarCipher(message, characterShift)
		fmt.Println(encryptedMessage)
	},
}

func init() {
	rootCmd.AddCommand(caesarCmd)
	caesarCmd.Flags().StringP("message", "m", "", "Message to be encrypted")
	err := caesarCmd.MarkFlagRequired("message")
	if err != nil {
		return
	}
	caesarCmd.Flags().IntP("shift", "i", 4, "Number of characters to shift")
}

func caesarCipher(message string, shift int) string {
	alphabet := "abcdefghijklmnopqrstuvwxyz"
	// How many chars in do we want to shift? (Between 1 and 25), 0 and 26 would yield no change.
	// This is what the shift variable is for

	// Take the length of the alphabet and subtract the count we're shifting to figure out how much to chop off
	chopped := alphabet[len(alphabet)-shift:]
	// Modify the alphabet, removing the components from the back the string
	remainingAlphabet := alphabet[:len(alphabet)-shift]
	// Reassemble the alphabet with the components modified
	crypt := chopped + remainingAlphabet

	byteSlice := []rune(message)
	var encryptedMessageArray []string
	for _, slice := range byteSlice {
		// Find the index of the character from the alphabet, we need pre-moved positions of the current string
		x := strings.IndexRune(alphabet, slice)
		// If there's not a match we need to just simply inject this character into the final string and skip everything
		// As the char is not something we know how to handle, spaces, quotes, brackets etc.
		//If x is -1 it's out of range
		if x == -1 {
			// Stringify my slice then directly
			nonAlphabeticCharacter := string(slice)
			encryptedMessageArray = append(encryptedMessageArray, nonAlphabeticCharacter)
		} else {
			// Lookup that in the index
			charByte := crypt[x]
			y := string(charByte)
			encryptedMessageArray = append(encryptedMessageArray, y)
		}
	}
	encryptedMessage := strings.Join(encryptedMessageArray, "")
	return encryptedMessage
}
