package crypto_cli_commands

import (
	"testing"
)

func Test_generateVigenereOverlayLong(t *testing.T) {
	key := "test"
	repeatLength := 10
	expected := "testtestte"
	actual := generateVigenereOverlay(key, repeatLength)
	if actual != expected {
		t.Errorf("Expected %s, got %s", expected, actual)
	} else {
		t.Logf("Expected %s, got %s", expected, actual)
	}
}

func Test_generateVigenereOverlayShort(t *testing.T) {
	key := "test"
	repeatLength := 3
	expected := "tes"
	actual := generateVigenereOverlay(key, repeatLength)
	if actual != expected {
		t.Errorf("Expected %s, got %s", expected, actual)
	} else {
		t.Logf("Expected %s, got %s", expected, actual)
	}
}

func Test_vigenereCipher01(t *testing.T) {
	message := "abc"
	key := "cba"
	expected := "ddd"
	actual, _ := vigenereCipher(message, key)
	if actual != expected {
		t.Errorf("Expected %s, got %s", expected, actual)
	} else {
		t.Logf("Expected %s, got %s", expected, actual)
	}
}

func Test_vigenereCipher02(t *testing.T) {
	message := "aBc"
	key := "cba"
	expected := "dDd"
	actual, _ := vigenereCipher(message, key)
	if actual != expected {
		t.Errorf("Expected %s, got %s", expected, actual)
	} else {
		t.Logf("Expected %s, got %s", expected, actual)
	}
}

func Test_vigenereCipher03(t *testing.T) {
	message := "A Z"
	key := "aaa"
	expected := "B A"
	actual, _ := vigenereCipher(message, key)
	if actual != expected {
		t.Errorf("Expected %s, got %s", expected, actual)
	} else {
		t.Logf("Expected %s, got %s", expected, actual)
	}
}

func Test_vigenereCipher04(t *testing.T) {
	message := "This ain't it man"
	key := "aaa"
	expected := "Uijt bjo'u ju nbo"
	actual, _ := vigenereCipher(message, key)
	if actual != expected {
		t.Errorf("Expected %s, got %s", expected, actual)
	} else {
		t.Logf("Expected %s, got %s", expected, actual)
	}
}

func Test_vigenereCipher05(t *testing.T) {
	message := "This ain't it man"
	key := "thisisakeylol"
	expected := "Nprl tjy's xf ujg"
	actual, _ := vigenereCipher(message, key)
	if actual != expected {
		t.Errorf("Expected %s, got %s", expected, actual)
	} else {
		t.Logf("Expected %s, got %s", expected, actual)
	}
}

// Provided tests are not valid. Leave commented out for now
//func TestNeilVigenereCipher(t *testing.T) {
//	tests := []struct {
//		name      string
//		message   string
//		key       string
//		want      string
//		wantError bool
//	}{
//		{
//			name:    "Key and message same length",
//			message: "abc",
//			key:     "cba",
//			want:    "EQNVZ",
//		},
//		{
//			name:    "Key shorter than message",
//			message: "HELLOWORLD",
//			key:     "XMCKL",
//			want:    "EQNVZWIJTA",
//		},
//		{
//			name:    "Key longer than message",
//			message: "HELLO",
//			key:     "XMCKLXMCKL",
//			want:    "EQNVZ",
//		},
//		{
//			name:      "Empty key",
//			message:   "HELLO",
//			key:       "",
//			wantError: true,
//		},
//		{
//			name:      "Empty message",
//			message:   "",
//			key:       "XMCKL",
//			wantError: true,
//		},
//	}
//
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			got, err := vigenereCipher(tt.message, tt.key)
//			if (err != nil) != tt.wantError {
//				t.Errorf("vigenereCipher() error = %v, wantErr %v", err, tt.wantError)
//				return
//			}
//			if got != tt.want {
//				t.Errorf("vigenereCipher() = %v, want %v", got, tt.want)
//			}
//		})
//	}
//}
