package crypto_cli_commands

import (
	"testing"
)

func Test_removeNonAZ(t *testing.T) {
	expected := "ABCD"
	actual := removeNonAZ("A B C D")
	if actual != expected {
		t.Errorf("Expected %s, got %s", expected, actual)
	} else {
		t.Logf("Expected %s, got %s", expected, actual)
	}
}

func TestGetCoordinate(t *testing.T) {
	tests := []struct {
		matrix      [][]rune
		charToFind  rune
		expectedRow int
		expectedCol int
	}{
		{
			matrix: [][]rune{
				{'A', 'B', 'C'},
				{'D', 'E', 'F'},
				{'G', 'H', 'I'},
			},
			charToFind:  'E',
			expectedRow: 1,
			expectedCol: 1,
		},
		{
			matrix: [][]rune{
				{'A', 'B', 'C'},
				{'D', 'E', 'F'},
				{'G', 'H', 'I'},
			},
			charToFind:  'G',
			expectedRow: 2,
			expectedCol: 0,
		},
		{
			matrix: [][]rune{
				{'A', 'B', 'C'},
				{'D', 'E', 'F'},
				{'G', 'H', 'I'},
			},
			charToFind:  'X',
			expectedRow: -1,
			expectedCol: -1,
		},
		{
			matrix: [][]rune{
				{'X', 'Y', 'Z'},
				{'L', 'M', 'N'},
			},
			charToFind:  'M',
			expectedRow: 1,
			expectedCol: 1,
		},
	}

	for _, test := range tests {
		t.Run(string(test.charToFind), func(t *testing.T) {
			row, col := getCoordinate(test.matrix, test.charToFind)
			if row != test.expectedRow || col != test.expectedCol {
				t.Errorf("getCoordinate(%v, %q) = (%d, %d); want (%d, %d)",
					test.matrix, test.charToFind, row, col, test.expectedRow, test.expectedCol)
			}
		})
	}
}
