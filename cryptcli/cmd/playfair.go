/*
Copyright © 2024 KURT BOMYA
*/
package crypto_cli_commands

import (
	"fmt"
	"github.com/spf13/cobra"
	"regexp"
	"strings"
)

// caesarCmd represents the caesar command
var playfairCmd = &cobra.Command{
	Use:   "playfair",
	Short: "Apply the playfair cipher to a message and return it",
	Long: `This flag takes a string that you want to encrypt along
with a key that will apply the playfair cipher`,
	Run: func(cmd *cobra.Command, args []string) {
		message, err := cmd.Flags().GetString("message")
		if err != nil {
			fmt.Println(err)
		}

		key, err := cmd.Flags().GetString("key")
		if err != nil {
			fmt.Println(err)
		}

		encryptedMessage, err := playfairCipher(message, key)
		if err != nil {
			return
		}
		fmt.Println(encryptedMessage)
	},
}

// RunePair defines a pair of runes to be used in the playfair cipher since it counts on pairs of runes to encrypt
type RunePair struct {
	FirstRune  rune
	SecondRune rune
}

func init() {
	rootCmd.AddCommand(playfairCmd)
	playfairCmd.Flags().StringP("message", "m", "", "Message to be encrypted")
	err := playfairCmd.MarkFlagRequired("message")
	if err != nil {
		return
	}
	playfairCmd.Flags().StringP("key", "k", "", "The playfair key to be used")
}

func hasDuplicateChars(keyToValidate string) bool {
	charMap := make(map[rune]int)

	for _, char := range keyToValidate {
		if _, exists := charMap[char]; exists {
			return true
		}
		charMap[char] = 1
	}
	return false
}

// Given a 5x5 matrix containing runes, return the coordinates of the given rune in the provided matrix
func getCoordinate(matrix [][]rune, charToFind rune) (int, int) {
	for i, row := range matrix {
		for j, char := range row {
			if char == charToFind {
				return i, j
			}
		}
	}
	return -1, -1
}

// Given a string, remove all characters in that string that don't fall between capital A and capital Z
func removeNonAZ(input string) string {
	regexApply := regexp.MustCompile("[^A-Z]")
	return regexApply.ReplaceAllString(input, "")
}

func playfairCipher(message string, key string) (string, error) {
	if len(message) == 0 {
		return "", fmt.Errorf("message cannot be empty")
	}
	if len(key) == 0 {
		return "", fmt.Errorf("key cannot be empty")
	}

	// Force both incoming strings to be uppercase, handle the entire cipher in uppercase
	message = strings.ToUpper(message)
	messageLettersOnly := removeNonAZ(message)
	key = strings.ToUpper(key)
	// Throw an error if the key contains multiple instances of the same letter
	if hasDuplicateChars(key) {
		return "", fmt.Errorf("the playfair key in particular cannot contain duplicate characters")
	}

	// Function defined variables
	alphabetWithoutJ := "ABCDEFGHIKLMNOPQRSTUVWXYZ" // Playfair cipher uses only 25 letters for the 5x5 grid and traditionally drops J

	// Reorganize the string used in the grid so that the key is at the front of the string and the remaining characters are appended

	for _, char := range key {
		alphabetWithoutJ = strings.Replace(alphabetWithoutJ, string(char), "", 1)
	}
	matrixString := key + alphabetWithoutJ

	// Define a 5x5 matrix of runes (characters) using slices
	matrix := make([][]rune, 5)
	for i := range matrix {
		matrix[i] = make([]rune, 5)
	}

	// Fill the matrix with characters from the string
	for i := 0; i < 5; i++ {
		for j := 0; j < 5; j++ {
			matrix[i][j] = rune(matrixString[i*5+j])
		}
	}

	// TODO: REMOVE THIS DEBUG
	// Print the matrix to verify
	for i := 0; i < 5; i++ {
		for j := 0; j < 5; j++ {
			fmt.Printf("%c ", matrix[i][j])
		}
		fmt.Println()
	}

	// Break the message into digraphs (pairs of letters)
	digraphs := make([]RunePair, 0)
	for i := 0; i <= len(messageLettersOnly)-1; {
		// If the second character is nothing, then we're at the end of the string
		// In this condition, the playfair cipher will add an X as the second character
		if i+1 > len(messageLettersOnly)-1 {
			digraphs = append(digraphs, RunePair{FirstRune: rune(messageLettersOnly[i]), SecondRune: 'X'})
			i += 1
		} else if messageLettersOnly[i] == messageLettersOnly[i+1] {
			digraphs = append(digraphs, RunePair{FirstRune: rune(messageLettersOnly[i]), SecondRune: 'X'})
			i += 1
		} else {
			// All other non-rule breaking, X inducing
			digraphs = append(digraphs, RunePair{FirstRune: rune(messageLettersOnly[i]), SecondRune: rune(messageLettersOnly[i+1])})
			i += 2
		}
	}

	// TODO: REMOVE DEBUG
	// Print each digraph
	for _, pair := range digraphs {
		fmt.Printf("Digraph: [%c, %c]\n", pair.FirstRune, pair.SecondRune)
	}

	var encryptedMessage string

	// Loop through the digraphs
	for _, pair := range digraphs {
		// Get the coordinates of the first character in the pair
		firstRow, firstCol := getCoordinate(matrix, pair.FirstRune)
		// Get the coordinates of the second character in the pair
		secondRow, secondCol := getCoordinate(matrix, pair.SecondRune)

		if firstCol == secondCol {
			// PLAYFAIR RULE 1: If in same COLUMN Move each letter down by 1
			if firstRow < 4 {
				// Go down by 1 row
				firstRow += 1
			} else {
				// You're already at the bottom of the column, wrap around to the first row
				firstRow = 0
			}
			encryptedMessage = encryptedMessage + string(matrix[firstRow][firstCol])
			// First letter is now handled, onto the second letter

			if secondRow < 4 {
				// Go down by 1 row
				secondRow += 1
			} else {
				// You're already at the bottom of the column, wrap around to the first row
				secondRow = 0
			}
			encryptedMessage = encryptedMessage + string(matrix[secondRow][secondCol])
			// The second letter is now handled, the next iteration can go
		} else if firstRow == secondRow {
			// PLAYFAIR RULE 2: If in same ROW Move each letter right by 1
			if firstCol < 4 {
				// Go down by 1 row
				firstCol += 1
			} else {
				// You're already at the bottom of the column, wrap around to the first row
				firstCol = 0
			}
			encryptedMessage = encryptedMessage + string(matrix[firstRow][firstCol])
			// First letter is now handled, onto the second letter

			if secondCol < 4 {
				// Go down by 1 row
				secondCol += 1
			} else {
				// You're already at the bottom of the column, wrap around to the first row
				secondCol = 0
			}
			encryptedMessage = encryptedMessage + string(matrix[secondRow][secondCol])
			// The second letter is now handled, the next iteration can go
		} else {
			// PLAYFAIR RULE 3: If neither, Form rectangle and choose opposite character
			encryptedMessage = encryptedMessage + string(matrix[firstRow][secondCol])
			encryptedMessage = encryptedMessage + string(matrix[secondRow][firstCol])
		}
	}

	return encryptedMessage, nil
}
