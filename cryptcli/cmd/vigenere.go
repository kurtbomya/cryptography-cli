/*
Copyright © 2024 KURT BOMYA
*/

package crypto_cli_commands

import (
	"fmt"
	"github.com/spf13/cobra"
	"strings"
	"unicode"
)

// vigenereCmd represents the vigenere command
var vigenereCmd = &cobra.Command{
	Use:   "vigenere",
	Short: "Apply the vigenere cipher to a message and return it",
	Long: `This flag takes a string that you want to encrypt along
with a key that will apply the vigenere cipher to`,
	Run: func(cmd *cobra.Command, args []string) {
		message, err := cmd.Flags().GetString("message")
		if err != nil {
			fmt.Println(err)
		}

		key, err := cmd.Flags().GetString("key")
		if err != nil {
			fmt.Println(err)
		}

		encryptedMessage, err := vigenereCipher(message, key)
		if err != nil {
			return
		}
		fmt.Println(encryptedMessage)
	},
}

func init() {
	rootCmd.AddCommand(vigenereCmd)
	vigenereCmd.Flags().StringP("message", "m", "", "Message to be encrypted")
	err := vigenereCmd.MarkFlagRequired("message")
	if err != nil {
		return
	}
	vigenereCmd.Flags().StringP("key", "k", "", "The vigenere key to be used")
}

// Given a key and a length, return a string that is the key repeated endlessly until it is the length
func generateVigenereOverlay(key string, repeatLength int) string {
	// Get the character length of the key
	keyLength := len(key)
	// If the key is shorter than the repeat length, we need to add the key to itself until it is at least the target length
	for keyLength <= repeatLength {
		// Check the length of the key again to see if it's long enough
		keyLength = len(key)
		key = key + key
	}
	// If the key started out longer than the repeat length, or if it became too long because the function above appended an inexact amount we need to chop off the back of the key
	if keyLength > repeatLength {
		key = key[:repeatLength]
	}
	return key
}

func vigenereCipher(message string, key string) (string, error) {
	if len(message) == 0 {
		return "", fmt.Errorf("message cannot be empty")
	}
	if len(key) == 0 {
		return "", fmt.Errorf("key cannot be empty")
	}
	key = strings.ToLower(key)
	// Get the length of the message
	stringLength := len(message)
	// Create a new slice of runes that is the same length as the message
	runeSeries := make([]rune, stringLength)
	// Convert message to individual runes, then load it into the slice array
	runeSeries = []rune(message)
	// Find every uppercase character and record its index in a slice
	caseSeries := make([]bool, stringLength)
	for i, char := range runeSeries {
		if char >= 65 && char <= 90 {
			caseSeries[i] = true
		} else {
			caseSeries[i] = false
		}
	}
	// Convert message to lowercase
	runeSeries = []rune(strings.ToLower(message))
	// Generate the overlay which we'll use to encrypt the message
	vigenereOverlay := generateVigenereOverlay(key, stringLength)
	// Create a rune series of that overlay, just like we did with our message
	runeSeriesOverlay := []rune(strings.ToLower(vigenereOverlay))
	// For each item in the runeSeriesOverlay, subtract 97 from it to get it into the range of a-z, set as differences from 1 to 26
	for i, _ := range runeSeriesOverlay {
		runeSeriesOverlay[i] = runeSeriesOverlay[i] - 97
	}

	// Add each position of runeSeriesOverlay to the corresponding position in runeSeries
	for i, _ := range runeSeries {
		if unicode.IsLetter(runeSeries[i]) { // If the result is a letter, do something
			// If the input started as lower case and ended up as anything else, we need to rope it back around to the start
			if unicode.IsLower(runeSeries[i]) && unicode.IsLower(runeSeries[i]+runeSeriesOverlay[i]+1) == false {
				// Plus 1 is to count the 'a' as 1 (instead of 'a' equalling zero and having no effect on encryption)
				// Minus 26 since we went out of range of the alphabet
				runeSeries[i] = runeSeries[i] + runeSeriesOverlay[i] + 1 - 26
			} else if unicode.IsLower(runeSeries[i]) && unicode.IsLower(runeSeries[i]+runeSeriesOverlay[i]+1) == true {
				// No need to shift alphabet here, we don't go out of range, so just add
				runeSeries[i] = runeSeries[i] + runeSeriesOverlay[i] + 1
			}
		} // otherwise it's a symbol and should be left alone
	}

	// If the new character is in the uppercase range, we need to lowercase it
	for i, _ := range runeSeries {
		if runeSeries[i] >= 65 && runeSeries[i] <= 90 {
			runeSeries[i] = unicode.ToLower(runeSeries[i])
		}
	}
	// Reapply the caseSeries to the runeSeries, bringing back the casing where it makes sense to
	for i, _ := range runeSeries {
		if caseSeries[i] {
			runeSeries[i] = unicode.ToUpper(runeSeries[i])
		}
	}
	encryptedMessage := string(runeSeries)
	return encryptedMessage, nil
}
