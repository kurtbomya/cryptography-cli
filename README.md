# cryptography-cli

Command line tool that implements various cryptography

## Usage

### caesarCipher

```bash
go run main.go caesar --message "it's not so bad" --shift 1
> hs'r mns rn azc
```

### vigenereCipher

- Vigenère, when pronounced by Americans sounds like "Vision-aire"

```bash
go run main.go vigenere -m "Hello there" -k "oil"
Wnxax iqqgn
```

### playfairCipher

```bash
go run main.go playfair -m "Hello there" -k "oil"
MCELAIQNDSML
```

### Tutorials Used
- https://github.com/spf13/cobra/blob/main/site/content/user_guide.md
- https://www.youtube.com/watch?v=-tO7zSv80UY